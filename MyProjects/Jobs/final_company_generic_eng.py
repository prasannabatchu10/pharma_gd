import requests, pyodbc
import json,re,ast,urllib
import datetime,time
#import pandas as pd
from scrapy.selector import Selector
import sql_server_db_AIE as sqlOp
from bs4 import BeautifulSoup
import datetime
check = "select MAX(TPhase),CStatus from [CompanyCareerPages_PaginationTPhase] with(nolock) group by CStatus"
snoRows = sqlOp.retrieveTabledata(check)
print(snoRows[0][1])
if snoRows[0][1] == None:
    TPhase = int(snoRows[0][0])
now = datetime.datetime.now()
dtm = now.strftime("%Y/%m/%d")

def pagination_deloite(cdmsid):
    getData = "select jobUrl,CDMSID,CompanyName,Sno from [dbo].[GenericSuccessCompanies2.5k] with(nolock) where PyResource like '%prasanna%' and CDMSID = " + str(cdmsid) + " order by Sno"
    queryData = sqlOp.retrieveTabledata(getData)
    for data in queryData:
        Sno = data[3]
        sUrl = data[0]
        cdmsid = data[1]
        compName = data[2]
        print("*************************")
        print(data[3])
        print(sUrl)
        print("******************")
    now = datetime.datetime.now()
    dtm = now.strftime("%Y/%m/%d")


    import requests
    response = requests.get('https://careersindia.deloitte.com/api/search/filter/%27%27/Y')
    jsn = response.json()
    jsn_data = jsn['jobs']
    for i in jsn_data:
        JobTitle =  (i['designation']+ ' - ' +i['department']+ ' - ' +i['functionalArea'])
        JobUrl = 'https://careersindia.deloitte.com/job-description/' + i['vacancyID']

        print("('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(str(JobTitle).replace("'","''"),"",str(JobUrl),str(sUrl),dtm,"",str(compName).replace("'","''").strip(),cdmsid,TPhase,Sno))
        sqlOp.insertRows("Insert Into [dbo].[CompanyCareerPagesGeneric_Pagination] (jobTitle,jobDesc,jobUrl,SiteName,publishDate,location,compName,CDMSID,TPhase,id) values('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')"
            .format(str(JobTitle).replace("'","''"),"",str(JobUrl),str(sUrl),dtm,"",str(compName).replace("'","''").strip(),cdmsid,TPhase,Sno))

def pagination_bnsf(cdmsid):
    getData = "select jobUrl,CDMSID,CompanyName,Sno from [dbo].[GenericSuccessCompanies2.5k] with(nolock) where PyResource like '%prasanna%' and CDMSID = " + str(cdmsid) + " order by Sno"
    queryData = sqlOp.retrieveTabledata(getData)
    for data in queryData:
        Sno = data[3]
        sUrl = data[0]
        cdmsid = data[1]
        compName = data[2]
        print("*************************")
        print(data[3])
        print(sUrl)
        print("******************")
    now = datetime.datetime.now()
    dtm = now.strftime("%Y/%m/%d")
    count = 0
    page = 0
    url = 'https://jobs.bnsf.com/search/?q=&sortColumn=referencedate&sortDirection=desc&startrow=%s' % (page)
    response = requests.get(url)
    sel = Selector(response)
    total_jobs = int(sel.xpath('//span[@class="paginationLabel"]/b/text()').extract()[1])  # pagination label
    print(total_jobs)
    jobs_in_page = 25
    if total_jobs % jobs_in_page == 0:
        page_count = total_jobs // jobs_in_page
    else:
        page_count = total_jobs // jobs_in_page + 1
    page = 0
    while page_count:
        url = 'https://jobs.bnsf.com/search/?q=&sortColumn=referencedate&sortDirection=desc&startrow=%s' % (page)
        print(url)
        response = requests.get(url)
        sel = Selector(response)
        nodes = sel.xpath('//span[@class="jobTitle visible-phone"]//a')
        if page > total_jobs:
            print("Pagination completed: %s" % page)
            break
        for node in nodes:
            job_title = ''.join(node.xpath('./text()').extract())
            job_url = 'https://jobs.bnsf.com' + ''.join(node.xpath('./@href').extract())
            print(job_title)
            print(job_url)
            count += 1

            print("('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(str(job_title).replace("'","''"),"",str(job_url),str(sUrl),dtm,"",str(compName).replace("'","''").strip(),cdmsid,TPhase,Sno))
            sqlOp.insertRows("Insert Into [dbo].[CompanyCareerPagesGeneric_Pagination] (jobTitle,jobDesc,jobUrl,SiteName,publishDate,location,compName,CDMSID,TPhase,id) values('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')"
                 .format(str(job_title).replace("'","''"),"",str(job_url),str(sUrl),dtm,"",str(compName).replace("'","''").strip(),cdmsid,TPhase,Sno))
        page += 25

def loadmore_sunoco(cdmsid):
    getData = "select jobUrl,CDMSID,CompanyName,Sno from [dbo].[GenericSuccessCompanies2.5k] with(nolock) where PyResource like '%prasanna%' and CDMSID = " + str(cdmsid) + " order by Sno"
    queryData = sqlOp.retrieveTabledata(getData)
    for data in queryData:
        Sno = data[3]
        sUrl = data[0]
        cdmsid = data[1]
        compName = data[2]
        print("*************************")
        print(data[3])
        print(sUrl)
        print("******************")
    now = datetime.datetime.now()
    dtm = now.strftime("%Y/%m/%d")
    cookies = {

        # 'JSESSIONID':'E82DE0F0D31D674012324FEB8C464F27.TC_948345_948333',
        # 'ORA_OTSS_SESSION_ID':'2cd771773e450e59579b3c5491f382c56e064df6988c03be345cc43c3e3d9458.energytrans.chprapo11412.tee.taleocloud.net',
        'JSESSIONID':'2B174893369C068E78DF5ABA031042B5.TC_948345_948333',
        'ORA_OTSS_SESSION_ID':'411fe3d62d1d149a9b8349b5638886dccbb044c70fe020b1090497a00e074970.energytrans.chprapo11412.tee.taleocloud.net',
    }

    headers = {
        'Connection': 'keep-alive',
        'Content-Length': '0',
        'Accept': 'application/json, text/javascript, */*; q=0.01',
        'X-Requested-With': 'XMLHttpRequest',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36',
        # 'tss-token': '1NEapql2WR477XyQmfjAERn/DLHfR9s5047hri3a3Yk=',
        'tss-token': 'BJWcw8GZ9gyj4/Xg4iiEOFXcUHXKQ2BFrw3fU3ccLq8=',
        'Origin': 'https://energytransfer.referrals.selectminds.com',
        'Sec-Fetch-Site': 'same-origin',
        'Sec-Fetch-Mode': 'cors',
        'Referer': 'https://energytransfer.referrals.selectminds.com/SUN/latest-jobs',
        'Accept-Encoding': 'gzip, deflate, br',
        'Accept-Language': 'en-US,en;q=0.9',
    }
    count = 0
    url = 'https://energytransfer.referrals.selectminds.com/SUN/latest-jobs'
    response = requests.get(url)
    sel = Selector(response)
    total_jobs = int(sel.xpath('//div[@class="number_of_results"]//p//strong//text()').extract()[0])
    print(total_jobs)
    jobs_in_page = 10
    if total_jobs % jobs_in_page == 0:
        page_count = total_jobs // jobs_in_page
    else:
        page_count = total_jobs // jobs_in_page + 1
    page = 1
    while page_count:
        response = requests.post(
            'https://energytransfer.referrals.selectminds.com/ajax/content/latest_job_results?page_num=%s&uid=541' % page,headers=headers, cookies=cookies)
        jsn_data = json.loads(response.text)
        res = jsn_data['Result']
        sel = Selector(text=res)
        nodes = sel.xpath('//a[@class="job_link font_bold"]')
        if page > page_count:
            print("Pagination completed: %s" % page)
            break
        for node in nodes:
            job_title = ''.join(node.xpath('./text()').extract())
            job_url = ''.join(node.xpath('./@href').extract())
            print("('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(str(job_title).replace("'", "''"), "",str(job_url), str(sUrl), dtm, "",str(compName).replace("'", "''").strip(),
                                                                               cdmsid, TPhase, Sno))
            sqlOp.insertRows("Insert Into [dbo].[CompanyCareerPagesGeneric_Pagination] (jobTitle,jobDesc,jobUrl,SiteName,publishDate,location,compName,CDMSID,TPhase,id) values('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')"
                .format(str(job_title).replace("'", "''"), "", str(job_url), str(sUrl), dtm, "", str(compName).replace("'", "''").strip(), cdmsid, TPhase, Sno))

            count += 1
        page += 1

def loadmore_energytransfer(cdmsid):
    getData = "select jobUrl,CDMSID,CompanyName,Sno from [dbo].[GenericSuccessCompanies2.5k] with(nolock) where PyResource like '%prasanna%' and CDMSID = " + str(cdmsid) + " order by Sno"
    queryData = sqlOp.retrieveTabledata(getData)
    for data in queryData:
        Sno = data[3]
        sUrl = data[0]
        cdmsid = data[1]
        compName = data[2]
        print("*************************")
        print(data[3])
        print(sUrl)
        print("******************")
    now = datetime.datetime.now()
    dtm = now.strftime("%Y/%m/%d")
    cookies = {
        # 'JSESSIONID': 'E4CE7A440061D38F63A3482A3C1F5FB7.TC_948345_948332',
        # 'ORA_OTSS_SESSION_ID': '66db623d3c0935d7fb584f3e2ff7a27727ac9a08a4d30d592ca838469737561b.energytrans.chprapo11312.tee.taleocloud.net',
        'ORA_OTSS_SESSION_ID':'5b3352f0b2991c29e85fe7d232c752880c28a59d9fd7c669cf45f45e6a0f73aa.energytrans.chprapo11412.tee.taleocloud.net',
        'JSESSIONID':'5BE7C8C6B918B42FCEAC2C105B8814BE.TC_948345_948333',
    }

    headers = {
        'Connection': 'keep-alive',
        'Content-Length': '0',
        'Accept': 'application/json, text/javascript, */*; q=0.01',
        'X-Requested-With': 'XMLHttpRequest',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36',
        # 'tss-token': '1NEapql2WR477XyQmfjAERn/DLHfR9s5047hri3a3Yk=',
        'tss-token': '25+U/3AhrsnDgpc25xL7+4kc/O9a+L3yze3dtv1HofI=',
        'Origin': 'https://energytransfer.referrals.selectminds.com',
        'Sec-Fetch-Site': 'same-origin',
        'Sec-Fetch-Mode': 'cors',
        'Referer': 'https://energytransfer.referrals.selectminds.com/latest-jobs',
        'Accept-Encoding': 'gzip, deflate, br',
        'Accept-Language': 'en-US,en;q=0.9',
    }

    response = requests.post('https://energytransfer.referrals.selectminds.com/ajax/content/latest_job_results',
                             headers=headers,cookies=cookies)
    count = 0
    url = 'https://energytransfer.referrals.selectminds.com/latest-jobs'
    response = requests.get(url)
    sel = Selector(response)
    total_jobs = int(sel.xpath('//div[@class="number_of_results"]//p//strong//text()').extract()[0])
    print(total_jobs)
    jobs_in_page = 10
    if total_jobs % jobs_in_page == 0:
        page_count = total_jobs // jobs_in_page
    else:
        page_count = total_jobs // jobs_in_page + 1
    page = 1
    while page_count:
        response = requests.post(
            'https://energytransfer.referrals.selectminds.com/ajax/content/latest_job_results?page_num=%s&uid=752'%page, headers=headers, cookies=cookies)
        jsn_data = json.loads(response.text)
        res = jsn_data['Result']
        sel = Selector(text=res)
        nodes = sel.xpath('//a[@class="job_link font_bold"]')
        if page > page_count:
            print("Pagination completed: %s" % page)
            break
        for node in nodes:
            job_title = ''.join(node.xpath('./text()').extract())
            job_url = ''.join(node.xpath('./@href').extract())
            print (job_title)
            print (job_url)
            print("('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(str(job_title).replace("'", "''"), "",str(job_url), str(sUrl), dtm, "",str(compName).replace("'", "''").strip(),
                                                                               cdmsid, TPhase, Sno))
            sqlOp.insertRows("Insert Into [dbo].[CompanyCareerPagesGeneric_Pagination] (jobTitle,jobDesc,jobUrl,SiteName,publishDate,location,compName,CDMSID,TPhase,id) values('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')"
                .format(str(job_title).replace("'", "''"), "", str(job_url), str(sUrl), dtm, "", str(compName).replace("'", "''").strip(), cdmsid, TPhase, Sno))

            count += 1
        page += 1

def json_cppid(cdmsid):
    getData = "select jobUrl,CDMSID,CompanyName,Sno from [dbo].[GenericSuccessCompanies2.5k] with(nolock) where PyResource like '%prasanna%' and CDMSID = " + str(cdmsid) + " order by Sno"
    queryData = sqlOp.retrieveTabledata(getData)
    for data in queryData:
        Sno = data[3]
        sUrl = data[0]
        cdmsid = data[1]
        compName = data[2]
        print("*************************")
        print(data[3])
        print(sUrl)
        print("******************")
    now = datetime.datetime.now()
    dtm = now.strftime("%Y/%m/%d")
    main_url='https://www.cppinvestments.com/career/experienced-professionals'
    response = requests.get('https://www.cppinvestments.com/wp-json/cppib/v1/smartrecruiter?orderby=job_name&lang=en&exp_level=exp')
    jsn = response.json()
    jsn_data = jsn['data']
    for i in jsn_data:
        job_title =  (i['job_name'])
        print (job_title)
        job_url =i['job_apply_url']
        print (job_url)

        print("('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(str(job_title).replace("'","''"),"",str(job_url),str(sUrl),dtm,"",str(compName).replace("'","''").strip(),cdmsid,TPhase,Sno))
        # sqlOp.insertRows("Insert Into [dbo].[CompanyCareerPagesGeneric_Pagination] (jobTitle,jobDesc,jobUrl,SiteName,publishDate,location,compName,CDMSID,TPhase,id) values('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')"
        #     .format(str(job_title).replace("'","''"),"",str(job_url),str(sUrl),dtm,"",str(compName).replace("'","''").strip(),cdmsid,TPhase,Sno))




json_cppid(1357609)
#pagination_bnsf(1659898)

#=============== Need to change session ids
#loadmore_energytransfer(1759020)

#====================Don't Run this ==============
#loadmore_sunoco(1704355)
#pagination_deloite(1617519)