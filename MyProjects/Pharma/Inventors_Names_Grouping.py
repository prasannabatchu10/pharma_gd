#!/usr/bin/env python
# coding: utf-8

# ### Import

# In[1]:


import pandas as pd
#import nltk
import re
#import spacy
#from flashtext import KeywordProcessor
#from nltk.corpus import stopwords
#stop_words = set(stopwords.words('english')) 
#pd.set_option('display.max_colwidth', -1)
#nlp= spacy.load('en_core_web_sm')
#from tqdm import tqdm_notebook as tqdm
import tensorflow
import keras
import pickle
import string
import ast
from tensorflow.keras.preprocessing.text import Tokenizer
from operator import itemgetter
import numpy as np
import itertools
import time


# ### SQL connection

# In[2]:


import pyodbc
con =pyodbc.connect('Driver={SQL Server};Server=PATENT;Database=PatentCMS;uid=patent;pwd=P@t3nt@123')
#con =pyodbc.connect('Driver={SQL Server};Server=Galaxy;Database=PatentsFrontend;uid=spiders;pwd=smart_123')
cursor = con.cursor()




import string
def clean_singles(text):
    toks=str(text).strip().title().split(' ')
    toks = [tok.strip() for tok in toks if tok not in ['',' ','  ','.',';',':','*']]
    sings=[tok for tok in toks if tok in list(string.ascii_uppercase)+list(string.ascii_lowercase)]
    toks = [tok for tok in toks if tok not in list(string.ascii_uppercase)+list(string.ascii_lowercase)]
    toks.append(''.join(sings))
    text = ' '.join(toks)
    text = text.title()
    return text


# In[5]:


def clean_txt(text):
    text = str(text).strip()
    text=re.sub(r"[./\[\]}{*^%\$@!`&),:;(~\"_“-”]",' ',text)
    #text=re.sub(r"[)(&]",' ',text)
    #text = re.sub(r"\'",'',text)
    text = re.sub(r"[^a-zA-Z0-9]",' ',text)
    words=text.split(' ')
    words = [wrd.strip() for wrd in words if len(wrd.strip())>0]
    text = ' '.join(words)
    text = re.sub(' +',' ',text)
    text = text.strip().title()
    return text

alphabets = list(string.ascii_uppercase)
for letter in alphabets:
	print('****',letter,'****')
	sql="select distinct * from tblcompany with(nolock) where StandardisedCompanyName like '"+letter+"%' and [Company Entity Type]='Company' order by [Company Name]"
	df = pd.read_sql(sql,con)
	df=df.sort_values(by=['StandardisedCompanyName'],ascending=True)
	df.to_pickle('Companies_'+letter+'.pkl')
	df['Cleaned']=df['StandardisedCompanyName'].apply(lambda x:clean_txt(x))
	df['Cleaned']=df['Cleaned'].apply(lambda x:clean_singles(x))
	df['Cleaned']=df['Cleaned'].apply(lambda x:clean_txt(x))

	df = df.drop_duplicates()
	df = df[df.Cleaned!='']
	df = df.dropna()
	df=df.reset_index(drop=True)
	
	tmp_dict = df.drop(['Cleaned'],axis=1).set_index('CompanyID')['StandardisedCompanyName'].to_dict()
	toks=' '.join(df['Cleaned'].tolist()).split(' ')
	toks = [tok.strip() for tok in toks if tok not in ['',' ','  ','.',';',':','*','&']]
	tokenizer = Tokenizer(lower=False,filters='')
	tokenizer.fit_on_texts(toks)
	freq=tokenizer.get_config()['word_counts']
	freq=ast.literal_eval(freq)
	sorted_freq = sorted(freq.items(), key=lambda kv: kv[1],reverse=False)

	weights = dict()
	for w,f in sorted_freq:
		weights[w]= 1/(np.log(f)+1)

	comp_list = df['Cleaned'].tolist()
	compids = df['CompanyID'].tolist()

	tokenizer = Tokenizer(lower=False,filters='')
	tokenizer.fit_on_texts(comp_list)

	seq_list = tokenizer.texts_to_sequences(comp_list)

	word_index=ast.literal_eval(tokenizer.get_config()['index_word'])

	index_dic=dict()
	for i in range(0,len(seq_list)):
		for idx in seq_list[i]:
			if word_index[str(idx)] in index_dic.keys():
				index_dic[word_index[str(idx)]].append(i)
			elif word_index[str(idx)] not in index_dic.keys():
				index_dic[word_index[str(idx)]]=[]
				index_dic[word_index[str(idx)]].append(i)

	comps_split = [comp.split(' ') for comp in comp_list]
	comps_weights=[]
	for tok in comps_split:
		comps_weights.append(sum([weights[w] for w in tok]))

	th = 0.80
	ind_list=[]
	for i in range(len(comp_list)):
		toks = comps_split[i]
		toks = sorted(toks, key=lambda x:weights[x],reverse=True)
		s1nots2 = []
		while 2*(1-th)*sum([weights[wrd] for wrd in toks]) >= th*(sum([weights[t] for t in s1nots2])):
			s1nots2.append(toks[0])
			toks.pop(0)
		j_index = sorted(list(set(itertools.chain.from_iterable([index_dic[tok] for tok in s1nots2]))))
		inds=[]
		for j in j_index:
			com = set(comps_split[i])&set(comps_split[j])
			if (2*sum([weights[w] for w in com])/(comps_weights[i]+comps_weights[j]))>th:
					inds.append(j)
		ind_list.append(inds)
		if i%(len(comp_list)/10)==0:
			print('#',end='\t')

	match_list=[]
	compid_list=[]
	for lst in ind_list:
		if len(lst)>1:
			compid_list.append(list(itemgetter(*lst)(compids)))
			match_list.append([tmp_dict[id] for id in list(itemgetter(*lst)(compids))])
		elif len(lst)==1:
			compid_list.append([compids[lst[0]]])
			match_list.append([tmp_dict[compids[lst[0]]]])

	parent_list=[]
	for l in range(len(match_list)):
		if len(match_list[l])>1:
			match_list[l] = sorted(match_list[l],key= lambda x:len(x),reverse=True)
			parent_list.append((compid_list[l][0],match_list[l][0]))
			del compid_list[l][0]
			del match_list[l][0]
		elif len(match_list[l])==1:
			parent_list.append((compid_list[l][0],match_list[l][0]))
	df_final = pd.DataFrame(parent_list,columns=['ParentID','Parent_Company'])

	df_final['CompanyID']=compid_list

	df_final = df_final.explode('CompanyID').reset_index(drop=True)

	df_final['Company_Name']=list(itertools.chain.from_iterable(match_list))

	df_final = df_final.drop_duplicates('Inventor_Name').reset_index(drop=True)
	df_final.to_pickle('Grouped_'+letter+'.pkl')
	print("Saved groups for letter "+letter)

# ### Writing into DB

# In[ ]:


'''
for i in tqdm(range(245,len(countries))):
    df = pd.read_pickle("./results/"+countries[i]+".pkl")
    df['Country']=countries[i]
    for index,row in tqdm(df.iterrows()):
        try:
            cursor.execute("INSERT INTO dbo.tblPatentInventor_grouped([ParentID],[Parent Name],[InventorID],[Inventor_Name],[Country]) values (?, ?,?,?,?)", row['ParentID'],row['Parent Name'], row['InventorID'],row['Inventor_Name'],row['Country'])
            con.commit()
        except Exception as e:
            exp = str(e).lower()
            if 'communication link failure' in exp:
                con =pyodbc.connect('Driver={SQL Server};Server=PATENT;Database=PatentCMS;uid=patent;pwd=P@t3nt@123')
                cursor = con.cursor()
'''
